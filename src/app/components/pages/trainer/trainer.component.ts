import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonApiService } from 'src/app/services/pokemon-api.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  pokemons : any = [];
  

  constructor(private pokeApi: PokemonApiService, private router: Router) { }

  ngOnInit(): void {
    this.findAllPokemonIds();    
  }
  //Gets all pokemon ids from local storage
  findAllPokemonIds() {
    let ids : any = [];
    let keys = Object.keys(localStorage);
    let i = keys.length;

    while ( i-- ) {
      //making sure trainer name is not being added.
      if (keys[i] != 'trainerName') ids.push(Number(localStorage.getItem(keys[i])));
    }
    this.fetchPokemons(ids);
  }

  //if card is clicked on, navigates to details of pokemon. 
  getDetails(id){
    this.router.navigateByUrl(`/details/${id}`);
  }

  //release pokemon from localstorage if clicked on released. 
  releasePokemon(name, id) {
    localStorage.removeItem(String(id));
    alert(`${name} was released...`);
  }

  //getting pokemons from api. 
  fetchPokemons(ids){
    ids.forEach(id => {
      this.pokeApi.getPokemonByID(id).then(pmon => 
        this.pokemons.push({
          name: pmon.name,
          image: pmon.sprites.front_default,
          id: pmon.id,
          types: pmon.types
        })
      )
    });
  }

  //matching card color with first type of pokemon. 
  setCardColour(type) : string{
    switch (type) {
      case 'grass':
        return '#32CD32';
      case 'poison':
        return '#9370DB';
      case 'fire':
        return '#FF4500';
      case 'flying':
        return '#AFEEEE';
      case 'bug':
        return '#6B8E23';
      case 'dark':
        return '#191919';
      case 'dragon':
        return '#483D8B';
      case 'electric':
        return '#FFD700';
      case 'fighting':
        return '#800000';
      case 'ice':
        return '#87CEEB';
      case 'psychic':
        return '#FF1493';
      case 'water':
        return '#1E90FF';  
      case 'normal':
        return '#808080';
      case 'steel':
        return '#778899';
      case 'rock':
        return '#CD853F';
      case 'ground':
        return '#D2691E';
      case 'fairy':
        return '#EE82EE';
      case 'ghost':
        return '#663399';  
      default:
        break;
    }
  }

}
