import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonApiService } from 'src/app/services/pokemon-api.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  id : number = 0; 
  pokemon : any; 

  constructor(private route: ActivatedRoute, private pokeApi: PokemonApiService, private router : Router) { 
    //Gets the id of the pokemon from the routing params. 
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.displayPokemon(this.id);
  }

  async displayPokemon(id){
    //Getting data from api. 
    await this.pokeApi.getPokemonByID(id).then(pmon => 
        this.pokemon = {
          name: pmon.name,
          height: pmon.height,
          weight: pmon.weight,
          image: pmon.sprites.front_default,
          types: pmon.types,
          stats: pmon.stats,
          moves: pmon.moves,
          abilities: pmon.abilities,
          id: pmon.id,
          basexp: pmon.base_experience,
        }
    );
    console.log(this.pokemon);
  }

  ngOnInit(): void {
  }

  //catches the pokemon if the pokemon is not already in the trainer compartement. 
  catchPokemon(id): void {
    if(localStorage.getItem(String(id))) alert("You already hold this Pokemon!");
    else{
      localStorage.setItem(String(id), String(id));
      alert('Pokemon cathed!');
      this.router.navigateByUrl('/catalouge');
    }
  }
}
