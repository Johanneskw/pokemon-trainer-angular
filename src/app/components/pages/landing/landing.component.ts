import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
 
  constructor(private router: Router) { }
  //gets name from  input field on button click, then navigates to catalouge. 
  getName(name){
    console.log(name);
    localStorage.setItem("trainerName", name);
    this.router.navigateByUrl("/catalouge");
  }
  //if there is a trainer name in localstroage, always nav to catalouge. 
  ngOnInit(): void {
    if(localStorage.getItem("trainerName")){
      this.router.navigateByUrl("/catalouge");
    }
  }

}
