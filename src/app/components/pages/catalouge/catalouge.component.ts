import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonApiService } from '../../../services/pokemon-api.service';

@Component({
  selector: 'app-catalouge',
  templateUrl: './catalouge.component.html',
  styleUrls: ['./catalouge.component.css']
})
 
export class CatalougeComponent implements OnInit {
  pokemons : any = [];
  indx : number = 0;
  showLoadingClass : boolean = false;

  constructor(private pokeApi: PokemonApiService, private router: Router) {}


  async ngOnInit() {
    //if the user has not yet set a trainer-name, no access to the catalouge.
    if(!localStorage.getItem('trainerName')) this.router.navigateByUrl('/');
    this.fetchPokemons(this.indx);
  }
 
  async fetchPokemons(indx) {
    await this.pokeApi.getPokemons(indx).then(res =>
      res.results.forEach(async pmon => {
        await this.pokeApi.getPokemon(pmon.url).then( pinfo =>
          //adding pokemons to list in the correct order. 
          this.insertSorted(this.pokemons,{
            id: pinfo.id,
            name: pinfo.name,
            imgsrc: pinfo.sprites.front_default,
            types: pinfo.types
          }, this.compareById)
        )
      })
    );
  }

  async searchPokemons(searchword) {
    //Clean the list of pokemons first;
    this.pokemons =[];
    await this.pokeApi.getPokemonsNoLimit().then(res =>
      res.results.forEach(async pmon => {
        if(pmon.name.includes(searchword.toLowerCase())){
          await this.pokeApi.getPokemon(pmon.url).then( pinfo =>
            //adding pokemons to list in the correct order. 
            this.insertSorted(this.pokemons,{
              id: pinfo.id,
              name: pinfo.name,
              imgsrc: pinfo.sprites.front_default,
              types: pinfo.types
            }, this.compareById)
          )
        }
      })
    );
  }

  getDetails(id){
    this.router.navigateByUrl(`/details/${id}`);
  }
  //inserts to array sorted on comparator (id).
  insertSorted (array, element, comparator) {
    for (var i = 0; i < array.length && comparator(array[i], element) < 0; i++) {}
    array.splice(i, 0, element)
  }
  //compares pokemons by id.
  compareById (a, b) { 
    return a.id - b.id 
  }

  //fetching next or prev pokeons on button click, always empty array before fetch.  
  navigateNext(){
    this.indx += 96;
    this.pokemons = [];
    this.fetchPokemons(this.indx);
  }
  navigatePrev(){
    this.indx -= 96;
    this.pokemons = [];
    this.fetchPokemons(this.indx);  
  }
  //getting color matched on first type of pokemon
  setCardColour(type) : string{
    switch (type) {
      case 'grass':
        return '#32CD32';
      case 'poison':
        return '#9370DB';
      case 'fire':
        return '#FF4500';
      case 'flying':
        return '#AFEEEE';
      case 'bug':
        return '#6B8E23';
      case 'dark':
        return '#191919';
      case 'dragon':
        return '#483D8B';
      case 'electric':
        return '#FFD700';
      case 'fighting':
        return '#800000';
      case 'ice':
        return '#87CEEB';
      case 'psychic':
        return '#FF1493';
      case 'water':
        return '#1E90FF';  
      case 'normal':
        return '#808080';
      case 'steel':
        return '#778899';
      case 'rock':
        return '#CD853F';
      case 'ground':
        return '#D2691E';
      case 'fairy':
        return '#EE82EE';
      case 'ghost':
        return '#663399';  
      default:
        break;
    }
  }

}
