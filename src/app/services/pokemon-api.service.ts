import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {

  constructor(private http:HttpClient){}

  //get 96 pokemons for cataouge
  getPokemons(indx) : Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon?limit=96&offset=${indx}`).toPromise();
  }
  //get one pokemon by url to get sprites and types for catalouge. 
  getPokemon(url) : Promise<any>{
    return this.http.get(url).toPromise();
  }
  //Get a pokemon by id for details. 
  getPokemonByID(id) : Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${id}`).toPromise();
  }
  //get all pokemons for search
  getPokemonsNoLimit() : Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon?limit=1050&offset=0`).toPromise();
  }

}
