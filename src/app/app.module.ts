import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from  '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatalougeComponent } from './components/pages/catalouge/catalouge.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';


@NgModule({
  declarations: [
    AppComponent,
    CatalougeComponent,
    LandingComponent,
    NavbarComponent,
    DetailComponent,
    TrainerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
